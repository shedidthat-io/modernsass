# Modern Sass

The purpose of this project is for anyone who wants to learn and practice Sass. I personally already know the foundational things and use it everyday in my role of Front-End Developer. However, I have a bias for continuous learning and education...technologies change <em>every</em> second! Additionally, this is a WordPress project, (the primary development language WordPress uses is called PHP) however, most of the technologies listed can be used on projects based off of many other programming languages and setups.

Additionally, the output of all the code written (the website) can be viewed by clicking the link named [rebel-cakes.localsite.io](https://rebel-cakes.localsite.io/). You will need a username and password to view, but I will share it if requested. I can be reached here on GitLab or either of the following social channels.

- [Instagram](https://www.instagram.com/shedidthat.io/)
- [TikTok](https://www.tiktok.com/@shedidthat.io)

## Credits and Links

I'm learning from multiple sources (and you can too). Here they are:

- **Sass YouTube Tutorial by The Net Ninja :** [Complete Sass Tutorial](https://youtube.com/playlist?list=PL4cUxeGkcC9jxJX7vojNVK-o8ubDZEcNb)
- **Gulp CSS Tricks Tutorial - Part 1 by Ali Alaa :** [Gulp for WordPress - Initial Setup](https://css-tricks.com/gulp-for-wordpress-initial-setup)
- **Gulp CSS Tricks Tutorial - Part 2 by Ali Alaa :** [Gulp for WordPress - Creating the Tasks](https://css-tricks.com/gulp-for-wordpress-creating-the-tasks/)

## The following technologies are used:

**For WordPress development**

- [Local By Flywheel](https://localwp.com/)

**To be able to use NPM, NVM, and JS based stuff**

- [Node.js](https://nodejs.org)

**To be able to install different packages(aka plugins/modules)**

- [NPM](https://www.npmjs.com/)

**To keep Node.js updated so other stuff will continue to work right**

- [NVM](https://github.com/nvm-sh/nvm#readme)

**To create web page elements in the browser**

- [HTML](https://www.w3.org/standards/webdesign/htmlcss)

**To style those very same elements**

- [CSS](https://www.w3.org/Style/CSS/)

**To allow your styles to be written in a more efficient, functional, and flexible way**

- [Sass](https://sass-lang.com/)

**To make elements and styles more interactive and dynamic**

- [JavaScript](https://www.javascript.com/) and/or
- [MDN Docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript)

**To be able to compile Sass**

- [Gulp](https://gulpjs.com/)

**To convert ECMAScript 2015+ code into a backwards compatible version of JavaScript in current and older browsers or environments**

- [Babel](https://babeljs.io/) and/or
- [What is Babel?](https://babeljs.io/docs/en/index.html)

**Markdown - the language syntax used to create this readme file**

- [Suggestions for a good README](https://www.makeareadme.com/#suggestions-for-a-good-readme) and/or
- [Markdown Guide](https://www.markdownguide.org/basic-syntax/)

**For Design and Imagery**

- [Figma](https://www.figma.com/)
- [Pexels](https://www.pexels.com/)
- [Undraw](https://undraw.co/illustrations)
- [FontAwesome](https://fontawesome.com/)
- [Google Fonts](https://fonts.google.com/)

## Description

Specifically, as stated in The Net Ninja's tutorial, a custom CSS Library is what will be produced at the end of this project.

**IMPORTANT**
All content and links listed were created by others, not me. I take no credit for any of the tutorials and/or links referrenced, as they are not mine. The people who created these tutorials worked very hard and all the props (pun intended) go to them!

The only thing I can take credit for is any copy on the front-end of the website that will be produced or any other integrations/features I decide to add along the way. Basically, anything I come up with will be clearly differentiated from the things I did not. In those cases, I will continue to add things, update this README, and give credit where credit is due.

## Badges

More on the way

<!-- On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge. -->

## Visuals

More on the way

<!-- Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method. -->

## Installation

More on the way

<!-- Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection. -->

## Usage

More on the way

<!-- Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README. -->

## Support

More on the way

<!-- Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc. -->

## Roadmap

More on the way

<!-- If you have ideas for releases in the future, it is a good idea to list them in the README. -->

## Contributing

More on the way

<!-- State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser. -->

## Authors and acknowledgment

More on the way

<!-- Show your appreciation to those who have contributed to the project. -->

## License

MIT

## Project status

Development is in progress as of May 5, 2022

<!-- If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers. -->

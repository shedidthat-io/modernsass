import named from 'vinyl-named';
import webpack from 'webpack-stream';
import { src, dest, watch, series, parallel } from 'gulp';
import del from 'del';
import postcss from 'gulp-postcss';
import sourcemaps from 'gulp-sourcemaps';
import autoprefixer from 'autoprefixer';
import yargs from 'yargs';
import sass from 'gulp-dart-sass';
import cleanCss from 'gulp-clean-css';
import gulpIf from 'gulp-if';
const PRODUCTION = yargs.argv.prod;

// gulp task compile scss to css task
export const styles = () => {
  return src(['src/scss/main.scss', 'src/scss/admin.scss'])
    .pipe(gulpIf(!PRODUCTION, sourcemaps.init()))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulpIf(PRODUCTION, postcss([ autoprefixer ])))
    .pipe(gulpIf(PRODUCTION, cleanCss({compatibility:'ie8'})))
    .pipe(gulpIf(!PRODUCTION, sourcemaps.write()))
    .pipe(dest('dist/css'));
}

// gulp task to watch for changes
export const watchForChanges = () => {
  watch('src/scss/**/*.scss', styles);
}

// gulp task that deletes the dist folder
export const clean = () => del(['dist']);

// gulp task for js scripts
export const scripts = () => {
  return src(['src/js/main.js','src/js/admin.js'])
  .pipe(named())
  .pipe(webpack({
    module: {
      rules: [
        {
          test: /\.js$/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        }
      ]
    },
    mode: PRODUCTION ? 'production' : 'development',
    devtool: !PRODUCTION ? 'inline-source-map' : false,
    output: {
      filename: '[name].js'
    },
    externals: {
      jquery: 'jQuery'
    },
  }))
  .pipe(dest('dist/js'));
}

export const dev = series(clean, parallel(styles), watchForChanges)
export const build = series(clean, parallel(styles))
export default dev;

/* Key Notes from tutorial */
/*
One final notice for this task. Let’s assume we want multiple CSS bundles: one for front-end styles and one for WordPress admin styles. We can create add a new admin.scss file in the src/scss directory and pass an array of paths in the Gulpfile:
*/
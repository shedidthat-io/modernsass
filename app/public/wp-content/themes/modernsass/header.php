<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo wp_get_document_title(); ?></title>
    <?php wp_head(); ?>
</head>

<body>
    <header>
        <?php
        ?>
        <section>
            <div>
                <h1>Your vision is our <span>craft.</span></h1>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Impedit repellendus cumque minus odit. Officiis ad perferendis velit possimus in? Vel cum reiciendis aliquam eligendi iusto dolores quam sequi.</p>
                <button>Click to change paragraphs color</button>
            </div>
        </section>
    </header>

    <main>
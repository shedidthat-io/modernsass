<?php

function _themename_assets_1()
{
    wp_enqueue_style('_themename_styles_1', get_template_directory_uri() . '/dist/css/main.css', array(), '1.0.0', 'all');
    wp_enqueue_style('_themename_styles_1', get_template_directory_uri() . '/dist/css/admin.css', array(), '1.0.0', 'all');
    wp_enqueue_script('_themename-scripts_1', get_template_directory_uri() . '/dist/js/main.js', array(), '1.0.0', false);
    wp_enqueue_script('_themename_addtl-scripts_1', get_template_directory_uri() . '/dist/js/main.js', array('jquery'), '3.6.0', true);
}

add_action('wp_enqueue_scripts', '_themename_assets_1');
